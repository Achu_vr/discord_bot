from discord.ext import commands
import config
import asyncio
import discord

bot = commands.Bot(command_prefix = "->")

@bot.event
async def on_ready():
    print("たけのこBotを起動します")



# メッセージを受信したら呼ばれる関数
@bot.event
async def on_message(message):
    if message.author == bot.user:
        return
        # Bot からのメッセージには反応しない
        # この判定をしないと無限ループが起きる

    # 事前に定義したNGワードが含まれたら、「NGワードですよ」と警告する
    NG_WORDS = ['死ね', 'おっぱい']
    for ng_word in NG_WORDS:
        if ng_word in message.content:
            embed = discord.Embed()
            embed.color = discord.Color.red()
            embed.description = f"**{ng_word}** はNGワードです。"
            await message.channel.send(embed=embed)
            break

    await bot.process_commands(message)

# メンバーが増えた時に呼ばれる関数
@bot.event
async def on_member_join(member):
    channel = bot.get_channel(config.INVITE_CHANNEL_ID)   
    await channel.send(
        f"{member.name} さん、きのぶち窓へようこそ！\n"
        f"わたしは「{bot.user.name}」といいます。\n"
        f"Discordが初めての方は、初めましての方へチャンネルをよく呼んでおくことをおすすめします。\nこのサーバーでは、特に決まり事はありません。ごゆっくりどうぞ！"
    )

bot.load_extension("cogs.hello")
bot.load_extension("cogs.notify")
bot.load_extension("cogs.help")
bot.load_extension("cogs.move_all_members")
bot.load_extension("cogs.help_markdown")
bot.load_extension("cogs.smash")
bot.load_extension("cogs.mute_unmute")
bot.run(config.TOKEN)
