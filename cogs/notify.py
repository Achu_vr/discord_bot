from discord.ext import commands
from discord.ext import tasks
from datetime import datetime

class Notify(commands.Cog):
    def __init__(self, bot):
        self.bot = bot
        self.notifier.start()
        self.channel = None

    def cog_unload(self):
        self.notifier.cancel()

    @tasks.loop(hours=24.0)
    async def notifier(self):

        # 日付をターミナルに記録
        now = datetime.now()
        print(f"今日は、{now.strftime('%Y/%m/%d')}です。")

        if self.channel != None:
            # 誕生日のリマインダー #########################################
            birthday_list_txt_load_path = "./birthday/birthday_list.txt"
            with open(birthday_list_txt_load_path) as f:
                for line in f:
                    split_info = line.split(' ')
                    # 0...name 1...month 2...day

                    if len(split_info) >= 3:
                        # ここから本日と各人の誕生日の照らし合わせ
                        if now.month == int(split_info[1]) and now.day == int(split_info[2]):
                            await self.channel.send(f"本日{now.month}月{now.day}日はなんと！ {split_info[0]}さんの誕生日です！\nおめでとうございますっ！")

                    else:
                        # 日がわからない人がいるので
                        if now.month == int(split_info[1]):
                            await self.channel.send(f"今月{now.month}月は、{split_info[0]}さんの誕生月です！\n日がわからないけどもおめでとう！")
            ############################################################

            # サーバーに入った日記念のリマインダー ###########################
            the_first_day_they_met_list_txt_load_path = "./birthday/the_first_day_they_met_list.txt"
            with open(the_first_day_they_met_list_txt_load_path) as f:
                for line in f:
                    split_info = line.split(' ')
                    # 0...name 1...month 2...day
                    
                    if now.month == int(split_info[1]) and now.day == int(split_info[2]):
                        await self.channel.send(f"本日{now.month}月{now.day}日はなんと！ {split_info[0]}さんがこのサーバーに入ってくれた日です！いつもありがとうっ！")
            ###########################################################
        
    @commands.command()
    async def set_notify_channel(self, ctx):
        self.channel = ctx.channel
        await ctx.send(f"{ctx.channel.name} チャンネルに時報を通知します")

def setup(bot):
    bot.add_cog(Notify(bot))