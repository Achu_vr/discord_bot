from discord.ext import commands

class Hello(commands.Cog):
    def __init__(self, bot):
        self.bot = bot


    @commands.command()
    async def hello(self, ctx):
        def check_message_author(msg):
            return msg.author is ctx.author
        await ctx.send(f"こんにちは、{ctx.author.name}さん")

def setup(bot):
    bot.add_cog(Hello(bot))
