from discord.ext import commands
import discord
import asyncio

class MoveMembers(commands.Cog):
    def __init__(self, bot):
        self.bot = bot

    @commands.command()
    async def move_all(self, ctx, before_channel: discord.VoiceChannel, after_channel: discord.VoiceChannel):
        def check_message_author(msg):
            return msg.author is ctx.author
        
        true_count = 0
        for channel in ctx.guild.channels:
            if(channel == before_channel):
                true_count += 1
            if(channel == after_channel):
                true_count += 1
        if true_count == 2:
            members = before_channel.members
            for member in members:
                await member.move_to(after_channel)
            await ctx.send(f"{before_channel}チャンネルにいたメンバーを{after_channel}チャンネルへ移動させました")
                

def setup(bot):
    bot.add_cog(MoveMembers(bot))