from discord.ext import commands

class HelpTakenoko(commands.Cog):
    def __init__(self, bot):
        self.bot = bot
        
    @commands.command()
    async def help_bot(self, ctx):
        def check_message_author(msg):
            return msg.author is ctx.author
        await ctx.send(
            f"**〜このボットの使い方〜**\n\n"
            f"**●できること**\n"
            f"・新しいメンバーが増えた時に、あいさつします\n"
            f"・チャットで```->hello``` とチャットすると挨拶をかえしてくれます\n"
            f"・誕生日をお祝いしてくれます\n"
            f"・このサーバーに入った日をお祝いしてくれます\n"
            f"・NGワードを言う人に注意をしてくれます\n"
            f"・スマブラの指定したキャラの情報を教えてくれます\n"
            f"\n"
            f"**●コマンド一覧**\n"
            f"・ヘルプ表示(これ)```->help_bot```\n"
            f"・あいさつ```->hello```\n"
            f"・Discord Markdown記法チートシート表示```->help_md```\n"
            f"・ボイスチャンネルにいる人を全員指定したボイスチャンネルへ移動させる```->move_all current_channel after_channel```\n"
            f"・ボイスチャンネルにいる人を全員ミュートさせる```->mute```\n"
            f"・ミュートさせたのを解除する```->unmute```\n"
            f"・スマブラの指定キャラの情報を見る```->smash character_name```名前が複雑なファイターは名前に含まれる文字列を指定すれば自動で補完してくれます。(例：Wii Fit トレーナー → Wii)\n"
            f"\n"
        )

def setup(bot):
    bot.add_cog(HelpTakenoko(bot))