from discord.ext import commands

class HelpMarkdown(commands.Cog):
    def __init__(self, bot):
        self.bot = bot

    @commands.command()
    async def help_md(self, ctx):
        def check_message_author(msg):
            return msg.author is ctx.author
        
        await ctx.send(
            f"●太字\n"
            f"テキストを2つのアスタリスク(*)で囲むと**太字**になります。\nなお、iOSのみ日本語での太字には対応していません。\n"
            f"\*\*kinoko\*\*\n"
            f"↓\n"
            f"**kinoko**\n\n"

            f"●斜体\n"
            f"テキストをアスタリスクかアンダースコア(_)で囲むと斜体になります。\n"
            f"なお、一部Android端末を除き日本語での斜体には対応していません。\n"
            f"\_kinoko\_\n"
            f"↓\n"
            f"_kinoko_\n\n"

            f"●下線\n"
            f"テキストを2つのアンダースコアで囲むと下線で装飾されます。\n"
            f"\_\_kinoko\_\_\n"
            f"↓\n"
            f"__kinoko__\n\n"

            f"●打ち消し線\n"
            f"テキストを2つのチルダ(~)で囲むと打ち消し線になります。\n"
            f"\~\~kinoko\~\~\n"
            f"↓\n"
            f"~~kinoko~~\n\n"

            f"●スポイラータグ(ネタバレ注意タグ)\n"
            f"厳密にはMarkdownではないが、テキストを2つのパイプ(|)で囲むと、入力するテキストが非表示になり、クリックすると表示されます。\n"
            f"\|\|kinoko\|\|\n"
            f"↓\n"
            f"||kinoko||\n\n"
            
            f"●インラインコードブロック\n"
            f"テキストをバッククォート(\`)で囲むとインラインコードブロックで表示されます。\n"
            f"\`kinoko\`\n"
            f"↓\n"
            f"`kinoko`\n\n"

            f"●コードブロック\n"
            f"テキストを3つのバッククォートで囲むとコードブロックで表示されます。\n"
            f"コードブロック内ではエンターキーのみで改行可能です。\n"
            f"\`\`\`kinoko\`\`\`\n"
            f"↓\n"
            f"```kinoko```\n\n"

            f"●引用\n"
            f"テキストの前に大なり記号＋半角スペース(> )を挿入すると、テキストを引用することができます。\n"
            f"\> kinoko\n"
            f"↓\n"
            f"> kinoko\n"
        )

def setup(bot):
    bot.add_cog(HelpMarkdown(bot))