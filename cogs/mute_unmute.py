from discord.ext import commands
import discord

client = discord.Client()
bot = discord.ext.commands.Bot(command_prefix = "->")

class MuteUnMute(commands.Cog):
    def __init__(self, bot):
        self.bot = bot
        

    @bot.command(pass_context=True)
    async def mute(self, ctx):
        # 通話にいるメンバーの取得
        def check_message_author(msg):
            return msg.author is ctx.author
        if ctx.author.voice:        
            members = ctx.author.voice.channel.members
        else:
            print("このコマンドはボイスチャンネルに入っている時のみ利用可能です。")
            await ctx.send("このコマンドはボイスチャンネルに入っている時のみ利用可能です。")
            return

        # ボイスチャンネルにいる人をサーバーミュートする
        for mem in members:
            await mem.edit(mute=True)
        print("Muted")


    @bot.command(pass_context=True)
    async def unmute(self, ctx):
        def check_message_author(msg):
            return msg.author is ctx.author
        if ctx.author.voice:
            members = ctx.author.voice.channel.members
        else:
            print("このコマンドはボイスチャンネルに入っている時のみ利用可能です。")
            await ctx.send("このコマンドはボイスチャンネルに入っている時のみ利用可能です。")
            return
        for mem in members:
            await mem.edit(mute=False)

def setup(bot):
    bot.add_cog(MuteUnMute(bot))