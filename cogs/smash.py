from discord.ext import commands
import requests
from bs4 import BeautifulSoup
import re

class SmashInfo(commands.Cog):
    def __init__(self, bot):
        self.bot = bot

    @commands.command()
    async def smash(self, ctx, character_name):
        def check_message_author(msg):
            return msg.author is ctx.author
        
        if character_name == "world" or character_name == "世界":
            smash_world_rate_req = requests.get("https://kumamate.net/vip/")
            rate_soup = BeautifulSoup(smash_world_rate_req.content, "html.parser")
            border = rate_soup.find('span', 'vipborder')
            reg = re.compile('[a-zA-Z!"#$%&\'\\\\()*+-./:;<=>?@[\\]^_`{|}~「」〔〕“”〈〉『』【】＆＊・（）＄＃＠。、？！｀＋￥％]')
            border_str = reg.sub('', border.__str__())
            await ctx.send(f"現在のVIPボーダー推定値は{border_str}です！\n")
            return

        # web scraping
        req = requests.get("https://w.atwiki.jp/smashsp_kensyou/pages/46.html")
        soup = BeautifulSoup(req.content, "html.parser")

        # 似通ったキャラのaタグを全部取得（マリオやドクターマリオなど）
        all_character_a = soup.find_all("a", text=re.compile(f"{character_name}"))
        # 似通った名前のキャラがいる場合
        count = 0
        a_split = []
        name = ''
        if all_character_a.__len__() >= 2:
            for a in all_character_a:
                a_str= a.__str__()
                a_split = a_str.split(' ')
                a_str = a_split[3]
                a_str = a_str.replace('\n', '')
                regex = re.compile('[0-9!"#$%&\'\\\\()*+,-./:;<=>?@[\\]^_`{|}~「」〔〕“”〈〉『』【】＆＊・（）＄＃＠。、？！｀＋￥％]')
                a_str = regex.sub('', a_str)
                a_str = a_str.replace("targetblank", '', 1).rstrip("a")
                if a_str == character_name:
                    name += a_str
                    break
                count += 1
        else:
            # 似通った名前のいないキャラクター
            a_str = all_character_a.__str__()
            a_split = a_str.split(' ')
            a_str = a_split[3]
            a_str = a_str.replace('\n', '')
            regex = re.compile('[0-9!"#$%&\'\\\\()*+,-./:;<=>?@[\\]^_`{|}~「」〔〕“”〈〉『』【】＆＊・（）＄＃＠。、？！｀＋￥％]')
            a_str = regex.sub('', a_str)
            a_str = a_str.replace("targetblank", '', 1).rstrip("a")
            name = f"{a_str} "
            
        # キャラクターネームにスペースが含まれるキャラクターの名前の作成
        if a_split.__len__() > 4:
            for n in a_split[4:a_split.__len__()]:
                n = n.replace('\n', '')
                n = n.replace('</a>]', '')
                name += f"{n} "
        
        # コマンドからの引数が一致したキャラクターのURLを取得し、HTMLを取得
        character_url = a_split[1].replace('href="', '').rstrip("\"")
        await ctx.send(f"{name}のフレームレート表です！\n{character_url}\n")
                
    
def setup(bot):
    bot.add_cog(SmashInfo(bot))